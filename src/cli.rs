use std::time::Duration;
use matrix_sdk::{
    ruma::{
        OwnedUserId, OwnedDeviceId,
    }
};
use clap::Parser;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Args {
    /// Homeserver to connect to
    pub homeserver_url: String,

    /// Username of the bot
    pub username: OwnedUserId,

    /// Token
    pub token: Option<String>,
    /// DeviceID
    pub device_id: Option<OwnedDeviceId>,

    /// Update frequency (default 30s)
    #[arg(short, long, value_parser = parse_duration)]
    pub frequency: Option<Duration>,

    /// Initial sending
    #[arg(short, long, default_value_t = false)]
    pub initial_sending: bool,
}

pub fn parse() -> Args {
    Args::parse()
}

fn parse_duration(arg: &str) -> Result<Duration, std::num::ParseIntError> {
    let seconds = arg.parse()?;
    Ok(Duration::from_secs(seconds))
}
