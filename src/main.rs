mod cli;
mod app;

use std::io;
use std::time::Duration;

use matrix_sdk::{
    self,
    room::Room,
    ruma::events::room::message::{
        MessageType, OriginalSyncRoomMessageEvent, RoomMessageEventContent, TextMessageEventContent,
    },
    ruma::{
        OwnedUserId, OwnedDeviceId,
    },
    Client,
    Session,
};
use url::Url;

async fn on_room_message(event: OriginalSyncRoomMessageEvent, room: Room) {
    if let Room::Joined(room) = room {
        if let OriginalSyncRoomMessageEvent {
            content:
                RoomMessageEventContent {
                    msgtype: MessageType::Text(TextMessageEventContent { body: msg_body, .. }),
                    ..
                },
            sender,
            ..
        } = event
        {
            let member = room.get_member(&sender).await.unwrap().unwrap();
            let name = member.display_name().unwrap_or_else(|| member.user_id().as_str());
            println!("{name}: {msg_body}");
        }
    }
}

async fn reauth(homeserver_url: String, username: &OwnedUserId, token: String, device_id: &OwnedDeviceId) -> matrix_sdk::Result<Client> {
    let client = Client::new(Url::parse(&homeserver_url)?).await?;

    let session = Session {
        access_token: token.clone(),
        refresh_token: None,
        user_id: username.clone(),
        device_id: device_id.clone(),
    };

    client.restore_login(session).await?;
    Ok(client)
}

async fn login_flow(homeserver_url: String, username: &OwnedUserId) -> matrix_sdk::Result<()> {
    let homeserver_url = Url::parse(&homeserver_url).expect("Couldn't parse the homeserver URL");
    let client = Client::new(homeserver_url).await.unwrap();

    print!("Password: ");
    let mut password = String::new();
    io::stdin().read_line(&mut password).expect("Unable to read user input");
    password = password.trim().to_owned();

    client.add_event_handler(on_room_message);

    let response = client
        .login_username(username, &password)
        .initial_device_display_name("rust-sdk")
        .send()
        .await?;

    println!(
        "token: {}, deviceid: {}",
        response.access_token, response.device_id
    );

    Ok(())
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    tracing_subscriber::fmt::init();

    let cli = cli::parse();

    match (cli.token, cli.device_id) {
        (Some(token), Some(device_id)) => {
            let client = reauth(cli.homeserver_url, &cli.username, token, &device_id).await?;
            let frequency = cli.frequency.unwrap_or(Duration::from_secs(30));
            app::run(client, frequency, cli.initial_sending).await?;
        },
        (_, _) => {
            login_flow(cli.homeserver_url, &cli.username).await?;
        }
    }


    Ok(())
}
