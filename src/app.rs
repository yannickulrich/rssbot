use tracing::info;
use matrix_sdk::{
    self,
    config::SyncSettings,
    room::{Room, Joined},
    ruma::RoomId,
    ruma::events::room::message::{
        RoomMessageEventContent,
        MessageType, OriginalSyncRoomMessageEvent,
        TextMessageEventContent,
    },
    ruma::events::{
        macros::EventContent,
    },
    event_handler::Ctx,
    Client,
};
use serde::{Deserialize, Serialize};
use std::collections::{
    HashMap, HashSet
};
use std::sync::Arc;
use tokio::sync::Mutex;
use reqwest;
use rss::{Channel, Item};
use anyhow::Result;
use std::time::Duration;
use tokio::{task, time};
use chrono::DateTime;

#[derive(Deserialize, Serialize, Debug, Clone, Eq, Hash, PartialEq)]
enum FormatOptions {
    #[serde(rename = "title")]
    TitleOnly,
    #[serde(rename = "description")]
    DescriptionOnly,
    #[serde(rename = "title+description")]
    TitleAndDescription,
    #[serde(rename = "author+title")]
    AuthorAndTitle,
    #[serde(rename = "author+description")]
    AuthorAndDescription,
    #[serde(rename = "author+title+description")]
    AuthorAndTitleAndDescription,
}

#[derive(Clone, Debug, Deserialize, Serialize, Eq, Hash, PartialEq)]
pub struct RssFeed {
    url: String,
    format: FormatOptions,
}

#[derive(Clone, Debug, Deserialize, Serialize, EventContent)]
#[ruma_event(type = "com.yannickulrich.rssbot.setup", kind = State, state_key_type = String)]
pub struct SetupEventContent {
    feeds: Vec<RssFeed>
}

type Sources = HashMap<RssFeed, HashSet<String>>;
type MutMap = Arc<Mutex<HashMap<String, Sources>>>;

async fn on_setup_event(event: OriginalSyncSetupEvent, room: Room, user_data: Ctx<(MutMap, bool)>) {
    if let Room::Joined(room) = room {
        let mut state = user_data.0.0.lock().await;
        let room_id = room.room_id().as_str().to_string();
        if let Some(srcs) = state.get_mut(&room_id) {
            for feed in event.content.feeds.iter() {
                if !srcs.contains_key(&feed) {
                    info!("Appending {} to {room_id}", feed.url);
                    srcs.insert(feed.clone(), HashSet::new());
                }
            }
            if user_data.0.1 {
                update_room(None, srcs).await.unwrap();
            }
        } else {
            info!("New room {room_id} with {} URLs", event.content.feeds.len());
            let mut srcs = event.content.feeds.iter().map(
                |f| (f.clone(),  HashSet::new())
            ).collect();
            if user_data.0.1 {
                update_room(None, &mut srcs).await.unwrap();
            }
            state.insert(room_id, srcs);
        }
    }
}

async fn send_item(room: &Joined, fmt: &FormatOptions, item: Item) -> Result<()> {
    info!("Sending item {:?} to room {:?}", item, room);
    let link = item.link.unwrap_or("#".to_string());
    let title = item.title.unwrap_or("News".to_string());
    let description = item.description.unwrap_or("empty".to_string());

    let author: String =
        if let Some(author) = item.author {
            author
        } else {
            if let Some(ext) = item.dublin_core_ext {
                if let Some(author) = ext.creators.get(0) {
                    author.clone()
                } else {
                    "someone".to_string()
                }
            } else {
                "someone".to_string()
            }
        };

    let body_plain = match fmt {
        FormatOptions::TitleOnly => format!("{title}"),
        FormatOptions::DescriptionOnly => format!("{description}"),
        FormatOptions::TitleAndDescription => format!("{title}: {description}"),
        FormatOptions::AuthorAndTitle => format!("{title} by {author}"),
        FormatOptions::AuthorAndDescription => format!("{author}: {description}"),
        FormatOptions::AuthorAndTitleAndDescription => format!("{title} by {author}: {description}"),
    };

    let body_html = match fmt {
        FormatOptions::TitleOnly => format!("<a href=\"{link}\">{title}</a>"),
        FormatOptions::DescriptionOnly => format!("<a href=\"{link}\">{description}</a>"),
        FormatOptions::TitleAndDescription => format!("<a href=\"{link}\"><b>{title}</b></a>: {description}"),
        FormatOptions::AuthorAndTitle => format!("<a href=\"{link}\">{title}</a> by <i>{author}</i>"),
        FormatOptions::AuthorAndDescription => format!("<a href=\"{link}\">{author}</a>: {description}"),
        FormatOptions::AuthorAndTitleAndDescription => format!("<a href=\"{link}\"><b>{title}</b></a> by <i>{author}</i>: {description}"),
    };

    room.send(
        RoomMessageEventContent::text_html(body_plain, body_html),
        None
    ).await?;
    Ok(())
}

async fn update_room(room: Option<&Joined>, srcs: &mut Sources) -> Result<()> {
    let mut to_send = vec![];

    for (feed, seen) in srcs.iter_mut() {
        info!("Fetching {:?}", feed.url);
        let content = reqwest::get(feed.url.clone())
            .await?
            .bytes()
            .await?;
        let channel = Channel::read_from(&content[..])?;
        for item in channel.items() {
            if let Some(id) = &item.guid {
                if let Some(date) = &item.pub_date {
                    if !seen.contains(&id.value) {
                        let date = DateTime::parse_from_rfc2822(&date).unwrap();
                        to_send.push((date, feed.format.clone(), item.clone()));
                        seen.insert(id.value.clone());
                    }
                }
            }
        }
    }

    if let Some(room) = room {
        to_send.sort_unstable_by_key(|l| l.0);
        for (_, fmt, item) in to_send.iter() {
            room.typing_notice(true).await?;
            send_item(&room, &fmt, item.clone()).await?;
        }
        room.typing_notice(false).await?;
    }
    Ok(())
}

async fn update(client: &Client, user_data: &MutMap) -> Result<()> {
    let mut state = user_data.lock().await;
    for (room_id, srcs) in state.iter_mut() {
        info!("Room {:?} has {} feeds", room_id, srcs.len());
        let my_room_id = <&RoomId>::try_from(room_id.as_str())?;
        let room = client.get_joined_room(my_room_id).unwrap();
        update_room(Some(&room), srcs).await?;
    }
    Ok(())
}

async fn update_loop(client: Client, mutex: MutMap, interval: Duration) -> Result<()> {
    let forever = task::spawn(async move {
        loop {
            time::sleep(interval).await;
            update(&client, &mutex).await.expect("Update");
        }
    });
    forever.await?;
    Ok(())
}

async fn on_room_message(event: OriginalSyncRoomMessageEvent, room: Room, user_data: Ctx<(MutMap, bool)>) {
    if let Room::Joined(room) = room {
        let msg_body = match event.content.msgtype {
            MessageType::Text(TextMessageEventContent { body, .. }) => body,
            _ => return,
        };

        if msg_body.contains("!help") {
            let body_plain = "RSS Matrix Bot\n\n\
                              * create a custom state event (com.yannickulrich.rssbot.setup) to configure feeds\n\
                              * licenced under AGPLv3\n\
                              * view https://gitlab.com/yannickulrich/rssbot for more information";
            let body_html = "<b>RSS Matrix Bot</b><p><ul>\
                              <li>create a custom state event (<code>com.yannickulrich.rssbot.setup</code>) to configure feeds</li>\
                              <li>licenced under AGPLv3</li>\
                              <li>view <a href=\"https://gitlab.com/yannickulrich/rssbot\">the repo</a> for more information</li>\
                              </ul>";

            room.send(
                RoomMessageEventContent::text_html(body_plain, body_html),
                None
            ).await.unwrap();
        } else if msg_body.contains("!update") {
            let mut state = user_data.0.0.lock().await;

            let room_id = room.room_id().to_string();
            if let Some(srcs) = state.get_mut(&room_id) {
                room.send(
                    RoomMessageEventContent::text_plain("forcing update"),
                    None
                ).await.unwrap();

                info!("Room {:?} has {} feeds", room.room_id(), srcs.len());
                update_room(Some(&room), srcs).await.unwrap();
            }
        }
    }
}


pub async fn run(client: Client, interval: Duration, initial_send: bool) -> Result<()> {
    let sync_token = client
        .sync_once(SyncSettings::default().full_state(true))
        .await
        .unwrap()
        .next_batch;

    let settings = SyncSettings::default().token(sync_token).full_state(true);

    let state: HashMap<String, HashMap<RssFeed, HashSet<String>>> = HashMap::new();
    let mutex = Arc::new(Mutex::new(state));
    client.add_event_handler_context((mutex.clone(), initial_send));
    client.add_event_handler(on_setup_event);
    client.add_event_handler(on_room_message);


    let (poll, sync) = tokio::join!(
        update_loop(client.clone(), mutex, interval),
        client.sync(settings)
    );
    poll?;
    sync?;

    Ok(())
}
