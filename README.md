# A Matrix RSS bot

This is a stateless matrix rss bot, similar to [Johni0702/matrix-rss-bot](https://github.com/Johni0702/matrix-rss-bot).
The feeds are not stored on disk but rather in the matrix homeserver as a custom room state event.

## Installation instruction
1. Build this project and compile or get [a pre-compiled binary](https://gitlab.com/yannickulrich/rssbot/-/jobs/artifacts/root/raw/public/rssbot?job=build)
1. Create an account for the RSS bot
1. Run `./rssbot <homeserver url> <username>` and type in the password
1. Take a note of the token and device ID
1. Run `./rssbot <homeserver url> <username> <token> <device id>`

## Usage instruction
1. Create a non-encrypted chat with the bot (for example by logging in in Element as the bot)
1. Create a custom event using the `/devtools` slash command in the chat with the bot
1. Click on `Explore room state`
1. Click on `Send custom state event`
1. As *Event Type*, write `com.yannickulrich.rssbot.setup` and as *State Key* something like `initial` or `data`
1. As the *Event Content*, enter the configuration.
   The `format` can be `title`, `description`, `title+description`, `author+title`,`author+description`, or `author+title+description` to control what part of the RSS feed is posted.
```json
{
    "feeds": [
        {
            "url": "<URL of RSS feed 1>",
            "format": "<format type 1>"
        },{
            "url": "<URL of RSS feed 2>",
            "format": "<format type 2>"
        },
        ...
    ]
}
```
7. You change the configuration by updating the state

Updates happen automatically (as often as specified by the `--frequency` flag, the default is 30s) or when you call the `!update` command in the room.
